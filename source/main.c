#include <stdio.h>
#include "dice.h"

#Este programa simula um lancamento de dados
int main(){
	int faces;
	initializeSeed();
	printf("Digite quantas faces o dado deve possuir: ");
	scanf("%d", &faces);
	printf("Let's roll the dice: %d\n", rollDice(faces));
	return 0;
}